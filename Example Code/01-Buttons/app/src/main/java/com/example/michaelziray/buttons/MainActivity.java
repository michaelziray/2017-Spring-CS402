package com.example.michaelziray.buttons;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String HAS_TAPPED = "mHasTapped";
    public static final String INTENT_KEY = "INTENT_KEY";
    private boolean mHasTapped = false;
    private TextView helloWorldTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.helloWorldTextView = (TextView) findViewById(R.id.hello_text_view);

        if (savedInstanceState != null) {
            Log.d("MZ", "saved instance is not null");

            mHasTapped = savedInstanceState.getBoolean(HAS_TAPPED, false);
            if (mHasTapped == true) {
                setLabelAfterTap(helloWorldTextView);
            }
        }

        Log.d("MZ", "onCreate() called");

        Button submitButton = (Button) findViewById(R.id.submit_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("MZ", "Button Clicked!");
                setLabelAfterTap(helloWorldTextView);
                mHasTapped = true;

                Intent intent = new Intent(MainActivity.this, ChildActivity.class);
                intent.putExtra(INTENT_KEY, "this is a string of data");
                startActivityForResult(intent, 99);
            }
        });
    }

    private void setLabelAfterTap(TextView helloWorldTextView) {
        helloWorldTextView.setText("button was clicked!!");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putBoolean(HAS_TAPPED, mHasTapped);

        Log.d("MZ", "On Saved instance is being called");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) {
            return;
        }

        String returnDataString = data.getStringExtra(ChildActivity.RETURN_INTENT_KEY);
        helloWorldTextView.setText(returnDataString);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.d("MZ", "onStart Called");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.d("MZ", "onStop called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MZ", "onDestroy() called.");
    }
}
