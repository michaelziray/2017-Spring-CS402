package com.electronicarmory.webservices;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.R.attr.name;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        WebServices.loadBuildings(this);


        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BuildingAddedEvent buildingAddedEvent) {
        Log.d("MZ", "A building was added");


        ArrayList<Building>buildingsThatWereAdded = buildingAddedEvent.buildingsAddedList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(BuildingRemovedEvent buildingRemovedEvent) {
        Log.d("MZ", "A building was removed");
    }
}
