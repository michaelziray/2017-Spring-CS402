package com.electronicarmory.webservices;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/14/17.
 */

public class BuildingAddedEvent {
    public ArrayList<Building> buildingsAddedList;

    public BuildingAddedEvent(ArrayList<Building> buildingsAddedList) {
        this.buildingsAddedList = buildingsAddedList;
    }
}
