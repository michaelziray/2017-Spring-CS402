package com.electronicarmory.webservices;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/14/17.
 */
public class BuildingRemovedEvent {

    public BuildingRemovedEvent(ArrayList<Building> removedBuildings) {
        this.removedBuildings = removedBuildings;
    }

    public ArrayList<Building>removedBuildings;
}
