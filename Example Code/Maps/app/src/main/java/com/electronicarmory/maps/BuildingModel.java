package com.electronicarmory.maps;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by michael.ziray on 2/28/17.
 */

public class BuildingModel {

    String buildingName;
    String buildingDescription;

    LatLng buildingLocation;


    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getBuildingDescription() {
        return buildingDescription;
    }

    public void setBuildingDescription(String buildingDescription) {
        this.buildingDescription = buildingDescription;
    }

    public LatLng getBuildingLocation() {
        return buildingLocation;
    }

    public void setBuildingLocation(LatLng buildingLocation) {
        this.buildingLocation = buildingLocation;
    }
}
