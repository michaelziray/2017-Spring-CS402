package com.electronicarmory.maps;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/28/17.
 */

public class BuildingsController {


    private final static ArrayList<BuildingModel> buildingsList = new ArrayList<>();


    public static ArrayList<BuildingModel> getBuildings(){

        return  buildingsList;
    }

    public static void addBuildings( ArrayList<BuildingModel>newBuildings){
        buildingsList.addAll( newBuildings);

        EventBus.getDefault().post(new AddBuildingEvent());
    }


}
