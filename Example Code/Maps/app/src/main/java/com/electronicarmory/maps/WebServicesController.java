package com.electronicarmory.maps;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by michael.ziray on 2/28/17.
 */

public class WebServicesController {

    private static RequestQueue queue;

    public static void refreshBuildings( Context context){

        if( queue == null ){
            queue = Volley.newRequestQueue(context);
        }
        String url ="https://s3-us-west-2.amazonaws.com/electronic-armory/buildings.json";

// Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
//                        mTextView.setText("Response is: "+ response.substring(0,500));
                        Log.d("MZ", response.toString());


                        try {
                            JSONArray jsonResponse = new JSONArray(response);

                            BuildingsController.addBuildings(
                                    BuidlingsFactory.createBuildings(jsonResponse));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                mTextView.setText("That didn't work!");
                Log.d("MZ", error.toString());
            }
        });
// Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}
