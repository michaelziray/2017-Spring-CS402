package com.electronicarmory.maps;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {

    TextView titleTextView;
    TextView descriptionTextView;
    ImageView imageView;
    Button submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        titleTextView = (TextView)findViewById(R.id.title_text);
        descriptionTextView = (TextView)findViewById(R.id.description_text);
        submitButton = (Button)findViewById(R.id.add_button);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String title = titleTextView.getText().toString();
                String description = descriptionTextView.getText().toString();

                // TODO: Get user's location

                BuildingModel newBuilding = new BuildingModel();
                newBuilding.setBuildingName(title);
                newBuilding.setBuildingDescription(description);

                ArrayList<BuildingModel>newBuildingsArray = new ArrayList<BuildingModel>();
                newBuildingsArray.add(newBuilding);
                BuildingsController.addBuildings(newBuildingsArray);

                finish();
            }
        });
    }
}
