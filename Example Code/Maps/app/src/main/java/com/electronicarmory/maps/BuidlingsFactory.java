package com.electronicarmory.maps;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by michael.ziray on 2/28/17.
 */

public class BuidlingsFactory {

    public static ArrayList<BuildingModel> createBuildings(JSONArray buildingsJSONArray){

        ArrayList<BuildingModel>newBuildings = new ArrayList<>();


        for(int index = 0; index < buildingsJSONArray.length(); index++){


            try {
                JSONObject buildingJSONObject = (JSONObject)buildingsJSONArray.get(index);
                String name = buildingJSONObject.getString("name");

                BuildingModel newBuilding = new BuildingModel();
                newBuilding.setBuildingName( name );

                newBuildings.add(newBuilding);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return newBuildings;

    }
}
