package com.electronicarmory.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragmentView = (View)inflater.inflate(R.layout.fragment_main, container, false);

        TextView textView = (TextView) fragmentView.findViewById(R.id.hellow);

        textView.setText("I have a reference!");

        return fragmentView;

    }
}
