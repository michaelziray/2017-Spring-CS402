package com.electronicarmory.databases;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActiveAndroid.initialize(this);

        UserModel userModel = new Select()
                .from(UserModel.class)
//                .where("Category = ?", category.getId())
//                .orderBy("RANDOM()")
                .executeSingle();

        if( userModel == null ) {
            userModel = new UserModel();
            userModel.username = "MichaelZiray";

            EmailModel emailModel = new EmailModel();
            emailModel.username = "MichaelZiray";
            emailModel.service = "BoiseState.edu";

            userModel.email = emailModel;

            userModel.save();
            emailModel.save();
        }
    }
}
