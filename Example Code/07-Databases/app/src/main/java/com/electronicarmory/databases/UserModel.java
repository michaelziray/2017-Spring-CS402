package com.electronicarmory.databases;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

/**
 * Created by michael.ziray on 2/23/17.
 */

@Table(name = "User")
public class UserModel extends Model{

    @Column(name = "username")
    public String username;

    @Column(name = "email")
    public EmailModel email;
}
